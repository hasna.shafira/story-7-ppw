from django.test import TestCase, Client
from .views import index

# Create your tests here.
class TestStory7(TestCase):
    def test_url_story7(self):
        response = Client().get('/')
        self.assertEquals(response.status_code, 200)
    
    def test_template_story7(self):
        response = Client().get('/')
        self.assertTemplateUsed(response, 'index.html')
    
    def test_text_story7(self):
        response = Client().get('/')
        html_kembalian = response.content.decode('utf8')
        self.assertIn("Story 7", html_kembalian)
